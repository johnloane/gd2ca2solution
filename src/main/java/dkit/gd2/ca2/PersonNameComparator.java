package dkit.gd2.ca2;

import java.util.Comparator;

public class PersonNameComparator implements Comparator<Person> {
    private SortOrder sortOrder;
    public PersonNameComparator(SortOrder sortOrder){
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Person p1, Person p2){
        int direction = sortOrder.getDirection();
        return direction * p1.getName().compareToIgnoreCase(p2.getName());

    }
}
