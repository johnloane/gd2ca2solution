package dkit.gd2.ca2;

public interface IMeasurer {
    public double getValue(Object o);
}
