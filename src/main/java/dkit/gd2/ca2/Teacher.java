package dkit.gd2.ca2;

import java.util.Arrays;

public class Teacher extends Person {
    private int numCourses;
    private int yearsTeaching;
    private String[] courses;
    private static final int MAX_COURSES = 6;

    public Teacher(String name, String address) {
        super(name, address);
        this.numCourses = 0;
        this.yearsTeaching = 0;
        courses = new String[MAX_COURSES];
    }


    public void setYearsTeaching(int yearsTeaching) {
        this.yearsTeaching = yearsTeaching;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "numCourses=" + numCourses +
                ", yearsTeaching=" + yearsTeaching +
                ", courses=" + Arrays.toString(courses) +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        if (!super.equals(o)) return false;

        Teacher teacher = (Teacher) o;

        if (numCourses != teacher.numCourses) return false;
        if (yearsTeaching != teacher.yearsTeaching) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(courses, teacher.courses);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + numCourses;
        result = 31 * result + yearsTeaching;
        result = 31 * result + Arrays.hashCode(courses);
        return result;
    }

    /**
     * Return false if the course already exists
     */
    public boolean addCourse(String course){
        //Check if the course aleady exists
        for(int i=0; i < numCourses; i++){
            if(courses[i].equalsIgnoreCase(course)){
                return false;
            }
        }
        courses[numCourses] = course;
        numCourses++;
        return true;
    }

    /**
     * Return false of the course does not exist
     */
    public boolean removeCourse(String course){
        boolean found = false;
        //Look for the course index
        int courseIndex = -1;
        for(int i=0; i < numCourses; i++){
            if(courses[i].equalsIgnoreCase(course)){
                courseIndex = i;
                found = true;
                break;
            }
        }
        if(found){
            //Course found, now remove it
            //Remove the course and copy all indices above down by one
            for(int i =courseIndex; i < numCourses-1; ++i){
                courses[i] = courses[i+1];
            }
            numCourses--;
            return true;
        }
        else{
            return false;
        }
    }
}
