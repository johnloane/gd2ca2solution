package dkit.gd2.ca2;

public enum SortOrder {
    Ascending(1),
    Descending(-1);

    private int direction;
    public int getDirection(){
        return this.direction = direction;
    }

    SortOrder(int direction){
        this.direction = direction;
    }
}
