package dkit.gd2.ca2;

public class StudentGradeMeasurer implements IMeasurer {
    @Override
    public double getValue(Object o){
        Student s = (Student) o;
        return s.getAverageGrade();
    }
}
