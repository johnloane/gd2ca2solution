package dkit.gd2.ca2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MainApp {
    public static void main(String[] args) {
        ArrayList<Person> listOfPeople = new ArrayList<Person>();
        populateListOfPeople(listOfPeople);
        ArrayList<Student> listOfStudents = new ArrayList<Student>();
        populateListOfStudents(listOfStudents);
        System.out.println(listOfPeople);
        Collections.sort(listOfPeople, new PersonNameComparator((SortOrder.Descending)));
        System.out.println(listOfPeople);

        System.out.println("Find student with the highest grade");
        Student bestStudent = getHighestAverageGrade(listOfStudents, new StudentGradeMeasurer());
        System.out.println(bestStudent);

        Student readMichael = new Student("Test", "Test");
        try {
            readMichael.readStudentFromFile("michael.txt");
            System.out.println("Just read Michael from file");
            System.out.println(readMichael);
        }catch(IOException io){
            io.printStackTrace();
        }
    }

    public static Student getHighestAverageGrade(ArrayList<Student> listOfStudents, IMeasurer measure){
        double largest = Double.MIN_VALUE;
        double actualValue = 0;
        Student bestObj = null;

        for(Student s : listOfStudents){
            actualValue = measure.getValue(s);
            if(actualValue > largest){
                largest = actualValue;
                bestObj = s;
            }
        }
        return bestObj;
    }

    public static void populateListOfPeople(ArrayList<Person> listOfPeople){
        Student daniel = new Student("Daniel", "Dublin Rd, Dundalk");
        daniel.addCourseGrade("OOP", 60);
        daniel.addCourseGrade("Database systems", 68);
        daniel.addCourseGrade("Web Programming", 70);
        daniel.addCourseGrade("Calculus", 55);
        daniel.addCourseGrade("Maths 2", 62);
        daniel.addCourseGrade("3D Animation", 88);
        listOfPeople.add(daniel);

        Teacher andrew = new Teacher("Andrew", "Muirhevena Mor, Dundalk");
        andrew.setYearsTeaching(10);
        andrew.addCourse("OOP");
        andrew.addCourse("Maths");
        andrew.addCourse("Database Systems");
        andrew.addCourse("Immersive Tech");
        andrew.addCourse("Project");
        andrew.addCourse("Calculus");
        listOfPeople.add(andrew);
    }

    public static void populateListOfStudents(ArrayList<Student> listOfStudents){
        Student daniel = new Student("Daniel", "Dublin Rd, Dundalk");
        daniel.addCourseGrade("OOP", 60);
        daniel.addCourseGrade("Database systems", 68);
        daniel.addCourseGrade("Web Programming", 70);
        daniel.addCourseGrade("Calculus", 55);
        daniel.addCourseGrade("Maths 2", 62);
        daniel.addCourseGrade("3D Animation", 88);
        listOfStudents.add(daniel);

        Student michael = new Student("Michael", "Dublin Rd, Dundalk");
        michael.addCourseGrade("OOP", 70);
        michael.addCourseGrade("Database systems", 68);
        michael.addCourseGrade("Web Programming", 70);
        michael.addCourseGrade("Calculus", 55);
        michael.addCourseGrade("Maths 2", 62);
        michael.addCourseGrade("3D Animation", 88);
        listOfStudents.add(michael);
        try {
            michael.writeStudentToFile("michael.txt");
        }catch(IOException io){
            io.printStackTrace();
        }

    }
}
