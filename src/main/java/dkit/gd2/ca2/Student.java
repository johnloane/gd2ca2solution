package dkit.gd2.ca2;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Student extends Person {
    private int numCourses;
    private String[] courses;
    private int[] grades;
    private static final int MAX_COURSES = 6;

    public Student(String name, String address) {
        super(name, address);
        this.numCourses = 0;
        courses = new String[MAX_COURSES];
        grades = new int[MAX_COURSES];
    }

    public int getNumCourses() {
        return numCourses;
    }

    public String[] getCourses() {
        return courses;
    }

    public int[] getGrades() {
        return grades;
    }

    public static int getMaxCourses() {
        return MAX_COURSES;
    }

    public void setNumCourses(int numCourses) {
        this.numCourses = numCourses;
    }

    @Override
    public String toString() {
        return "Student{" +
                "numCourses=" + numCourses +
                ", courses=" + Arrays.toString(courses) +
                ", grades=" + Arrays.toString(grades) +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;

        Student student = (Student) o;

        if (getNumCourses() != student.getNumCourses()) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getCourses(), student.getCourses())) return false;
        return Arrays.equals(getGrades(), student.getGrades());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getNumCourses();
        result = 31 * result + Arrays.hashCode(getCourses());
        result = 31 * result + Arrays.hashCode(getGrades());
        return result;
    }

    /**
     * Add a course and its grade
     * Notice no validation
     */
    public void addCourseGrade(String course, int grade){
        courses[numCourses] = course;
        grades[numCourses] = grade;
        ++numCourses;
    }

    public double getAverageGrade(){
        int sum = 0;
        double result = -1.0;
        for(int i=0; i < numCourses; i++){
            sum += grades[i];
        }
        try{
            result = (double)sum/numCourses;
        }catch(ArithmeticException ae){
            ae.printStackTrace();
        }
        return result;
    }

    public void writeStudentToFile(String filename) throws IOException {
        try(BufferedWriter studentFile = new BufferedWriter(new FileWriter(filename))){
            studentFile.write(this.getName() + ";" + this.getAddress() + ";" + this.getNumCourses() + ";");
            for(int i=0; i < numCourses; i++){
                studentFile.write(courses[i] + ";");
            }
            for(int i=0; i < numCourses; i++){
                studentFile.write(grades[i] + ";");
            }
        }
    }

    public void readStudentFromFile(String filename) throws IOException{
        try(Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)))){
            sc.useDelimiter(";");
            while(sc.hasNextLine()){
                String name = sc.next();
                this.setName(name);
                sc.skip(sc.delimiter());
                String address = sc.next();
                this.setAddress(address);
                sc.skip(sc.delimiter());
                int numCourses = sc.nextInt();
                //this.setNumCourses(numCourses);
                String[] courses = new String[numCourses];
                int[] grades = new int[numCourses];
                for(int i=0; i < numCourses; i++){
                    courses[i] = sc.next();
                    sc.skip(sc.delimiter());
                }
                for(int i=0; i < numCourses; i++){
                    grades[i] = sc.nextInt();
                    sc.skip(sc.delimiter());
                }
                for(int i=0; i < numCourses; i++){
                    this.addCourseGrade(courses[i], grades[i]);
                }
            }
        }
    }
}
